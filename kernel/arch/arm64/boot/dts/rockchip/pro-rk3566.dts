// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2020 Rockchip Electronics Co., Ltd.
 *
 */

#include "rk3566-evb1-ddr4-v10-linux.dts"
#include "rp-mipi-camera-gc2093-rk3566.dtsi"

#include "lcd-gpio-pro-rk3566.dtsi"			// lcd pins configuration


/***************** SINGLE LCD (LCD + HDMI) ****************/
/* HDMI */
//#include "rp-lcd-hdmi.dtsi"

/* MIPI DSI0 */
//#include "rp-lcd-mipi0-5-720-1280.dtsi"
//#include "rp-lcd-mipi0-5-720-1280-v2-boxTP.dtsi"
//#include "rp-lcd-mipi0-5.5-1080-1920.dtsi"
//#include "rp-lcd-mipi0-5.5-720-1280.dtsi"
//#include "rp-lcd-mipi0-5.5-720-1280-v2.dtsi"
#include "rp-lcd-mipi0-7-1024-600.dtsi"
//#include "rp-lcd-mipi0-7-1200-1920.dtsi"
//#include "rp-lcd-mipi0-8-800-1280-v2.dtsi"
//#include "rp-lcd-mipi0-8-800-1280-v3.dtsi"
//#include "rp-lcd-mipi0-8-1200-1920-v2.dtsi"
//#include "rp-lcd-mipi0-10-800-1280-v2.dtsi"
//#include "rp-lcd-mipi0-10-800-1280-v3.dtsi"
//#include "rp-lcd-mipi0-10-1920-1200.dtsi"
//#include "rp-lcd-mipi0-10-1200-1920.dtsi"

/* MIPI DSI1 */
//#include "rp-lcd-mipi1-7-1024-600.dtsi"
//#include "rp-lcd-mipi1-10-800-1280.dtsi"
//#include "rp-lcd-mipi1-7-1200-1920.dtsi"

/* LVDS */
//#include "rp-lcd-lvds-10-1024-600.dtsi"
//#include "rp-lcd-lvds-7-1024-600.dtsi"

/* EDP */
//#include "rp-lcd-edp-13-1920-1080.dtsi"
//#include "rp-lcd-edp-13.3-15.6-1920-1080.dtsi"



/ {

    rp_power {
        compatible = "rp_power";
        rp_not_deep_sleep = <1>;
        status = "okay";
    };


    rp_gpio{
        status = "okay";
        compatible = "rp_gpio";

        /******* SYSTEM POWER **********/
        gpio2b3 {	//3.3v 5v enable
            gpio_num = <&gpio2 RK_PB3 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };
        gpio3c2 {       //system led
            gpio_num = <&gpio3 RK_PC2 GPIO_ACTIVE_HIGH>;
            gpio_function = <3>;
        };
        gpio0b0 {       //usb hub reset
            gpio_num = <&gpio0 RK_PB0 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio4c4 {       //SPK ENABLE
            gpio_num = <&gpio4 RK_PC4 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio0a0 {       //SPK MUTE
            gpio_num = <&gpio0 RK_PA0 GPIO_ACTIVE_LOW>;
            gpio_function = <0>;
        };

        /***** SPI_FLASH ********/
        gpio1d0 {
            gpio_num = <&gpio1 RK_PD0 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };
        gpio1d1 {
            gpio_num = <&gpio1 RK_PD1 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio1d2 {
            gpio_num = <&gpio1 RK_PD2 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };
        gpio1d3 {
            gpio_num = <&gpio1 RK_PD3 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio1d4 {
            gpio_num = <&gpio1 RK_PD4 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        /***** PDM ********/

        gpio4a0 {
            gpio_num = <&gpio4 RK_PA0 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio4a1 {
            gpio_num = <&gpio4 RK_PA1 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };
        gpio4a2 {
            gpio_num = <&gpio4 RK_PA2 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };

        gpio4a3 {
            gpio_num = <&gpio4 RK_PA3 GPIO_ACTIVE_HIGH>;
            gpio_function = <0>;
        };
		
		otg_host {       //OTG SWITCH
			gpio_num = <&gpio0 RK_PC2 GPIO_ACTIVE_LOW>;
			gpio_function = <0>;
		};
    };
};


&i2c0 {
        vdd_cpu: tcs4525@1c {
                compatible = "tcs,tcs452x";
                reg = <0x1c>;
                vin-supply = <&vcc5v0_sys>;
                regulator-compatible = "fan53555-reg";
                regulator-name = "vdd_cpu";
                regulator-min-microvolt = <712500>;
                regulator-max-microvolt = <1390000>;
                regulator-ramp-delay = <2300>;
                fcs,suspend-voltage-selector = <1>;
                regulator-boot-on;
                regulator-always-on;
                regulator-state-mem {
                        regulator-off-in-suspend;
                };
        };
};

&hdmi_sound{
        status = "disabled";
};
&rk809_codec {
	hp-volume = <20>;	/* 3(max)-255 */
	spk-volume = <100>;	/* 3(max)-255 */
	capture-volume = <0>;	/* 0(max)-255 */
};

&i2c4 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&i2c4m1_xfer>;

    rtc@51 {
        status = "okay";
        compatible = "rtc,hym8563";
        reg = <0x51>;
    };
};

&uart3 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&uart3m0_xfer>;
};

&uart6 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&uart6m0_xfer>;
};

&uart7 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&uart7m0_xfer>;
};

&uart9 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&uart9m0_xfer>;
};

&spi1 {
    status = "okay";
};

&spi2 {
    status = "okay";
};

&spi3 {
    status = "disabled";
};

&dmc {
    status = "disabled";
};

&pdm{
	pinctrl-names = "default";
	pinctrl-0 = <&pdmm1_clk1
			     &pdmm1_sdi1
			     &pdmm1_sdi2
			     &pdmm1_sdi3>;
	status = "disabled";
};

&pmu_io_domains {
	status = "okay";
	pmuio1-supply = <&vcc3v3_pmu>;
	pmuio2-supply = <&vcc3v3_pmu>;
	vccio1-supply = <&vccio_acodec>;
	vccio3-supply = <&vccio_sd>;
	// vccio4-supply = <&vcc_1v8>; // gmac vccio 1.8v
	vccio4-supply = <&vcc_3v3>; // gmac vccio 3.3V
	vccio5-supply = <&vcc_3v3>;
	vccio6-supply = <&vcc_1v8>;
	vccio7-supply = <&vcc_3v3>;
};

